---
layout: job_page
title: "Head of strategic business development and go to market strategy"
---

## Responsibilities

- Strategic partnerships (integrations, container schedulers, hosting, channel, open source)
- Corporate positioning (conversational development, remote only)
- Investor relations (fund-raising, updates, addressing concerns)
- Business development

While we don't have marketing people dedicated this person can also help with:

- Analyst relations (corporate spokesperson)
- Community (open source) and other evangelism
- Public Relations

## Requirements

- Problem solver who can quickly master new concepts and subject matter.
- Understand the ALM market and all its players, from Docker to AWS, from Perforce to PTC.
- Excellent verbal and written communication skills.
- Relationship focused, a positive relation with our partners.
- Be able to represent the company in place of the CEO.
- Ability to work well with colleagues both in and outside of the organization, with good judgment, discretion, and tact.
- Ability to work independently and to undertake supervisory responsibilities as needed.
- You share our [values](/handbook/values), and work in accordance with those values
