---
layout: markdown_page
title: "Compensation and Title Changes"
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Title Changes

At GitLab, we encourage team members to take control of their own career ladders and career advancement. In all processes of title changes, the team member will speak with their manager about the change. Come prepared with an explanation of how you believe you meet the proposed level and can satisfy the business need.

As a manager, please follow the following processes and discuss any proposed changes with either the People Ops Business Partner or Senior Director of People Ops, and do not make promises to the team member before the approvals are completed.

### Promotions

To promote one of your direct reports, a manager should follow the following steps:

- Promotions are to be based on meeting the criteria of the role the individual is to be promoted in to (i.e. promote based on performance, not based on potential). If the criteria for promotion are not adequately described on the relevant job description page, then work on that first. If the job is being advertised via the [jobs page](https://about.gitlab.com/jobs/) in order to be compliant with global anti-discrimination laws the individual must apply for the role. Similarly, if there is no job posting, one must be created and shared on the team call so that everyone has the opportunity to apply and be considered.
- Manager must obtain agreement from their own manager(s), all the way through the chain of command to the CEO.
- Promotions should also include a review of [compensation](handbook/people-operations/global-compensation-calculator) and [stock options](handbook/stock-options/#stock-option-grant-levels). Managers should feel free to consult with People Operations on these topics; and of course always adhere to the Global Compensation Calculator. Don't send the promotion-proposal to the CEO until this part is included.
- Once written agreement is reached on the promotion and changes (if any) in compensation, the manager informs the individual.
- Once the individual has been informed, the manager informs People Operations with the promotion notice (i.e. reasons and related title and comp changes). People Operations processes the changes in relevant administrative systems.
- Promotions are then announced by the manager on the team call; where the manager describes how the individual met the promotion criteria and includes a link to the merge request where the individuals title is updated on the team page.
- If some amount of onboarding in the new role, or offboarding from the old role is required (for example a change in access levels to infrastructure systems; switch in groups and email aliases, etc.) make an associated issue on the [GitLab Organization issue tracker](https://gitlab.com/gitlab-com/organization/) to list and track progress on those topics.

### Demotions

To demote one of your direct reports, a manager should follow the following steps:

- The manager should discuss any performance issues or possible demotions with the People Ops Business Partner in their scheduled meetings.
- To initiate the process, the manager must obtain agreement from two levels of management.
- Proposed changes to a current job description or a new job description should be delivered with request for approval by the second level manager and the People Ops Business Partner.
- Demotions should also include a review of [compensation](handbook/people-operations/global-compensation-calculator) and [stock options](handbook/stock-options/#stock-option-grant-levels). Managers should feel free to consult with People Operations on these topics; and of course always adhere to the Global Compensation Calculator.
- Once written agreement is reached on the demotion and changes (if any) in compensation, send the agreement to the CEO for final approval.
- Once approved, the manager informs the individual. Please cc People Ops once the individual has been informed, to processes the changes in the relevant administrative systems.
- Changes in title are announced on the team call.
- The manager will initiate any necessary onboarding or offboarding.

### Department Transfers

If you are interested in a position, regardless of level, outside your department or general career progression, you can apply for a transfer.

- If you are interested in a transfer, first discuss this with your manager so that everyone is aware of the situation.
- Transfers must go through the application process for the new position by applying on the [Jobs page](http://about.gitlab.com/jobs). The team member will go through the entire interview process outlined on the job description. Any questions about the role or the process? Please reach out to People Ops to discuss.
- People Ops will ensure that, if applicable, the position has been posted for at least five business days before an offer is made.
- [Compensation](handbook/people-operations/global-compensation-calculator) and [stock options](handbook/stock-options/#stock-option-grant-levels) may be reviewed during the hiring process to reflect the new level and position.
- A new [contract](https://about.gitlab.com/handbook/contracts/#employee-contractor-agreements) will be sent out following the hiring process.
- If the team member is transferred, the new manager will announce on the team call and begin any additional onboarding or offboarding necessary.

### Internal Department Transfers

If you are interested in another position within your department and the hiring manager is also your manager you must do the following;

- Present your proposition to your manager.
- If the position is advertised on the [jobs page](https://about.gitlab.com/jobs/)) to be considered you must apply for it. If there is no job posting, one must be created and shared on the team call so that everyone has the opportunity to apply and be considered.
- The manager will asses the function requirements; each level should be defined in the job description.
- If approved, your manager will need to obtain approval from their manager, through the chain of command to the CEO.
- [Compensation](handbook/people-operations/global-compensation-calculator) and [stock options](handbook/stock-options/#stock-option-grant-levels) will be reevaluated to ensure it adheres to the compensation calculator. Don't send the proposal to the CEO until this part is included.
- If the team member is transferred, the manager will announce on the team call and begin any additional onboarding or offboarding necessary.

## Compensation and Title Changes Process

Any change to compensation or title requested by a manager must be approved by processing it in BambooHR. The Manager (Reports To) should initiate the change by logging into BambooHR to complete the approval workflow.  Once initiated, the workflow will push the transaction to the Manager's Manager, the People Ops Business Partner, the CEO, and the People Operations Administrator for processing.

If you have any questions throughout this process, please feel free to contact People Ops. People Ops will confirm the change as the last step in the approval process. This ensures that further changes may need to be [processed in TriNet](#changes-trinet) or HRSavvy to fully process the change. People Ops
is responsible for seeing the change through to completion. Once completed, People Ops
sends an email to the person reporting / requesting the change (member's manager or CEO)
to confirm this.

If you feel your title is not aligned with your skill level or you are interested in the career ladder ahead, you should discuss your personal professional development plan with your manager or the People Operations Generalist.

### Process for Compensation and Title Change in BambooHR

Manager (Reports To):

1. Login to BambooHR.
1. Select the team member you would like to adjust.
1. In the top right hand corner, click Request a Change.
1. Select which type of change you are requesting. If you are only looking to change compensation, then select Compensation. If this is a promotion that includes a title change and a compensation change, select Promotion. To only change the title, select Job Information.
1. Enter in all applicable fields in the form, and then submit.
1. If there was a title change associated with the request, People Ops will complete a merge request to change the title on the Team Page.

### TriNet

#### Record Pay Change

1. Go to HR Passport homepage.
1. Click Find.
1. Select find person by Name.
1. Type the name, click search.
1. Select Employment Data on the left side of the screen.
1. Click Employee Pay Change
1. Select Effective Date.
1. Enter new rates.
1. Answer the worker's compensation question.
1. Save.

#### Record Job Change

1. Go to HR Passport homepage.
1. Click Find.
1. Select find person by Name.
1. Type the name, click search.
1. Select Employment Data on the left side of the screen.
1. Select the reason for the change.
1. Enter the effective date.
1. Click next.
1. Select the reason.
1. Change the business title. Also, change any other applicable fields.
1. Enter yes or no under the worker's comp section.
1. Click Update
